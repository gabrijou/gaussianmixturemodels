# Gaussian mixture models for clustering and calibration of ensemble weather forecasts

## Privacy Statement
The contents of this file are subject to the CeCILL v2.1 license. You may not use this file except in compliance with the license terms. The original code is developed by SCALIAN DS. Portions of code created by SCALIAN DS are Copyright (C) SCALIAN DS. All right reserved. <br/>
<br/>
SCALIAN DS <br/>
417 L'Occitane - CS 77679, 31676 Labege Cedex  - France <br/>
Phone: +33 (0)5 61 00 79 79 <br/>
http://www.scalian.com <br/>
<br/>
For any commercial use of this code, contact SCALIAN.
<br/>

## Scientific purpose
<br/>
In this github repository, codes are proposed to fit Gaussian mixture models for ensemble data ![](https://render.githubusercontent.com/render/math?math=X_{i}=(x_{i1},\cdots,x_{iM})), ![](https://render.githubusercontent.com/render/math?math=\forall+i+\in\\{1,...,n\\}) where M denotes the ensemble size and n the sample size. 


A data set example in 2 dimensions is plotted below. 

<div class="image-wrapper" >
    <img src="/figures/example.png" alt=""/>
  </a>
      <p class="image-caption">Figure 1: Clustering objective with a bivariate ensemble data into K=3 classes.</p>
</div>

In the article, we present and discuss three different methods to fit ensemble data with the EM algorithm. They are refered to as Empirical statistics, Super sample and Exchangeable variables. Each method is implemented in 'GMMscripts.R' and briefly described below.

## Empirical statistics: 
The idea is to link the empirical statistics data estimated on ensemble ![](https://render.githubusercontent.com/render/math?math=S_{i}=(\overline{x}_i,s^2_i)) to the latent variable ![](https://render.githubusercontent.com/render/math?math=Z_{i}) characterising the ensemble data designed by a Gaussian mixture with K components. ![](https://render.githubusercontent.com/render/math?math=\overline{x}_i) represents the empirical mean and ![](https://render.githubusercontent.com/render/math?math=s^2_i) the variance.

<div class="image-wrapper" >
    <img src="/figures/empstatistics.png" alt=""/>
  </a>
      <p class="image-caption">Figure 2: Empirical statistics model.</p>
</div>


## Super sample: 
The simplest way to adapt the EM algorithm to ensemble data and take advantage of the large number of observations is to consider the ensemble as a super sample ![](https://render.githubusercontent.com/render/math?math=(x_{11},\cdots,x_{nM})) of ![](https://render.githubusercontent.com/render/math?math=n{\times}M) independent realisations of the Gaussian mixture.

<div class="image-wrapper" >
    <img src="/figures/supersample.png" alt=""/>
  </a>
      <p class="image-caption">Figure 3: Super sample model.</p>
</div>
To be able to predict a specific class for a new entering ensemble, a majority vote procedure is performed on the ensemble realisation.

## Exchangeable variable
 The alternative to the super sample approach is to consider the ensemble as a vector of variables ![](https://render.githubusercontent.com/render/math?math=(X_{1},\cdots,X_{M})) where ![](https://render.githubusercontent.com/render/math?math=X_{m}) is the m-th member and ![](https://render.githubusercontent.com/render/math?math=X_{m}) is assumed to be independent of ![](https://render.githubusercontent.com/render/math?math=X_{\ell}) if ![](https://render.githubusercontent.com/render/math?math=m\neq\ell), and exchangeable. A sequence of random variables is exchangeable
if its joint distribution is invariant under variable permutations. 

<div class="image-wrapper" >
    <img src="/figures/exchvariables.png" alt=""/>
  </a>
      <p class="image-caption">Figure 4: Exchangeable variables model.</p>
</div>


## Experimental setup

Two cases with K=4 clusters are considered with two different variable dimensions (d=1 and d=3). One of them is referred to as "regular" because it is similar to standard  examples of the literature. The marginal conditional distributions are plotted on the left panel of Figure 5. The clusters have distinct means and they are slightly overlapping. The second example is referred to as "difficult".  It is illustrated on the right panel of Figure 5. The clusters present a strong overlap. Two of them  have the same mean and different variances. Such a situation occurs in ensemble calibration problems when the ensembles are unbiased but  over (resp. under) dispersed. 

<div class="image-wrapper" >
    <img src="/figures/Simulation.png" alt=""/>
  </a>
      <p class="image-caption">Figure 5: Univariate case distribution.</p>
</div>

Gaussian component parameters are summarised in Table 1 for each case, with the same class proportion parameters.
  
<div class="image-wrapper" >
    <img src="/figures/table1.PNG" alt=""/>
</div>


## Results

The simulation implemented in the script GMMmain reproduces results displayed in the article for  ensemble data sets of size n=200 and ensemble size M=50.  Threrefore, the three models are fitted on the simulated ensemble data sets for different values of ![](https://render.githubusercontent.com/render/math?math=K+\in\\{2,...,6\\}). At the end, percent of selection of the number of classes K using BIC score, scores of RMSE (values close to zero mean accurate estimation of the true parameters) and accuracy (values going towards one show a good recovery of the true class serie Z by the model) are computed and displayed below .

### Regular
<div class="image-wrapper" >
    <img src="/figures/Regular_BIC.png" alt=""/>
  </a>
      <p class="image-caption">Figure 6: Percent of the classes number K selected by models with the BIC score.</p>
</div>
<div class="image-wrapper" >
    <img src="/figures/Regular_RMSE.png" alt=""/>
  </a>
      <p class="image-caption">Figure 7: Estimated RMSE by models.</p>
</div>
<div class="image-wrapper" >
    <img src="/figures/Regular_ACC.png" alt=""/>
  </a>
      <p class="image-caption">Figure 8: Estimated Accuracy by models.</p>
</div>


### Difficult
<div class="image-wrapper" >
    <img src="/figures/Difficult_BIC.png" alt=""/>
  </a>
      <p class="image-caption">Figure 9: Percent of the classes number K selected by models with the BIC score.</p>
</div>
<div class="image-wrapper" >
    <img src="/figures/Difficult_RMSE.png" alt=""/>
  </a>
      <p class="image-caption">Figure 10: Estimated RMSE by models.</p>
</div>
<div class="image-wrapper" >
    <img src="/figures/Difficult_ACC.png" alt=""/>
  </a>
      <p class="image-caption">Figure 11: Estimated Accuracy by models.</p>
</div>

